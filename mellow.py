import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
import asyncio
import asyncpg
import aiofiles
from datetime import datetime
try:
    import uvloop
except ImportError:
    pass
else:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

description = """mellow Bot"""


bot = commands.Bot(command_prefix=commands.when_mentioned_or('^^'), case_insensitive=True, description=description)
bot.launch_time = datetime.utcnow()
bot.remove_command('help')

async def create_pool():
    credentials = {"user": "", "password": "", "database": "", "host": "127.0.0.1"}
    pool = await asyncpg.create_pool(**credentials, max_size=100)
    return pool

startup_extensions = ['jishaku', 'modules.NSFW', 'modules.Help', 'modules.Fun', 'modules.Starboard', 'modules.utils.errorhandler']

@bot.event
async def on_ready():
    print("Bot ready!")
    await bot.change_presence(activity=discord.Streaming(name=f'with OwO', url='https://www.twitch.tv/mellowmarshe'))
    pool = await create_pool()
    bot.pool = pool
    async with aiofiles.open('modules/utils/schema.sql') as f:
        await bot.pool.execute(await f.read())

@bot.event
async def on_message(message):
    if not message.author.bot:
        await bot.process_commands(message)


@bot.command(hidden=True)
@commands.is_owner()
async def load(ctx, extension_name : str):
    try:
        bot.load_extension(extension_name)
    except (AttributeError, ImportError) as e:
        await ctx.send("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await ctx.send("{} loaded.".format(extension_name)) 

@bot.command(hidden=True)
@commands.is_owner()
async def unload(ctx, extension_name : str):
    bot.unload_extension(extension_name)
    await ctx.send("{} unloaded.".format(extension_name))

    if __name__ == "__main__":
        for extension in startup_extensions:
            try:
                bot.load_extension(extension)
            except Exception as e:
                exc = '{}: {}'.format(type(e).__name__, e)
                print('Failed to load extension {}\n{}'.format(extension, exc))


@bot.command(hidden=True)
@commands.is_owner()
async def die(ctx):
    await ctx.send('Bye cruel world...')
    await bot.logout()

if __name__ == "__main__":
    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extension {}\n{}'.format(extension, exc))

@bot.command(name='reload', hidden=True)
@commands.is_owner()
async def cog_reload(ctx, *, cog: str):
    bot.unload_extension(cog)
    bot.load_extension(cog)
    await ctx.send(f'**Successfuly reloaded {cog}**')

bot.run('')