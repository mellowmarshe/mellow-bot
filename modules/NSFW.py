import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
import aiohttp
import json
from .utils import checks
from .utils import lists

class NSFW:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
    description='''Get a NSFW image or gif from the specified type
    Can only be used in NSFW channels for an obvious reason.''', brief='NSFW images and gifs', usage='nsfw <type>')
    @checks.nsfw_channel()
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def nsfw(self, ctx, *, type:str=None):
        if type not in lists.possible_types or type is None:
            await ctx.send(f'Invalid type passed. The following are valid types: `{str(lists.possible_types)}`'.replace('[','').replace(']',''))
        else:
            async with aiohttp.ClientSession() as cs:
                async with cs.get(f'https://nekos.life/api/v2/img/{type}') as r:
                    res = await r.json()
                    embed = discord.Embed(color=0x6441a5)
                    embed.set_image(url=res['url'])
                    embed.set_author(name=f'{type}')
                    embed.set_footer(text=f'Requested by {ctx.author}')
                    await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(NSFW(bot))