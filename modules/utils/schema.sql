CREATE TABLE IF NOT EXISTS sb_guild_setting (
	guildid BIGINT PRIMARY KEY,
    sb_channel BIGINT NOT NULL,
    sb_min_limit INT NOT NULL DEFAULT 3,
    sb_selfstar Boolean NOT NULL
);

CREATE TABLE IF NOT EXISTS sb_storage (
    sb_entry_id  BIGINT PRIMARY KEY,
    guildid BIGINT NOT NULL,
    sb_channel_id BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS sb_stars (
    sb_star BIGINT REFERENCES sb_storage(sb_entry_id),
    sb_entry_author_id BIGINT NOT NULL, 
    PRIMARY KEY (sb_star, sb_entry_author_id)
);