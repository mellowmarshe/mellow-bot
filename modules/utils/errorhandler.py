import traceback
import sys
from discord.ext import commands
import discord

class errorhandler:
    def __init__(self, bot):
        self.bot = bot

    async def on_command_error(self, ctx, error):
        if hasattr(ctx.command, 'on_error'):
            return
        
        errors = (commands.MissingRequiredArgument, commands.BadArgument, commands.NoPrivateMessage, commands.CheckFailure, commands.DisabledCommand, commands.CommandInvokeError, commands.TooManyArguments, commands.UserInputError, commands.NotOwner, commands.MissingPermissions, commands.BotMissingPermissions)   
        error = getattr(error, 'original', error)
        
        if isinstance(error, errors):
            await ctx.send(f'An error has occured! Error: **{error}**')   
        else:
            print (error)

def setup(bot):
    bot.add_cog(errorhandler(bot))