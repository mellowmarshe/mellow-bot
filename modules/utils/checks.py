import discord
from discord.ext import commands
import asyncio
import asyncpg

def nsfw_channel():
    async def predicate(ctx):
        if ctx.message.channel.is_nsfw() is False:
            await ctx.send('NSFW commands may only be used in NSFW channels.')
        else:
            return True

    return commands.check(predicate)

def has_starboard():
    async def predicate(ctx):
        if await ctx.bot.pool.fetchrow('''SELECT * FROM sb_guild_setting WHERE guildid = $1;''', ctx.guild.id) is None:
            await ctx.send("You may not use this command because you don't have a starboard.")
        else:
            return True

    return commands.check(predicate)