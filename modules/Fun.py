import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
from .utils import checks
import random

class Fun:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='markov', 
    description='''Generates a markov chain either from the channel specified or channel where called
    Made by [Eviee](https://github.com/EvieePy/EvieeBot/blob/master/modules/fun.py#L343) | [License](https://github.com/EvieePy/EvieeBot/blob/master/LICENSE)''', 
    brief='Generates Markov chain', usage='markov [channel]')
    async def do_markov(self, ctx, *, channel: discord.TextChannel=None):
        if not channel:
            channel = ctx.channel

        msg = await ctx.send('Generating Markov')

        chain = {}
        try:
            data = ' '.join([m.clean_content for m in await channel.history(limit=1000).flatten()
                             if not m.author.bot]).replace('\n\n', ' ').split(' ')
        except discord.HTTPException:
            return await msg.edit(content='An error has occured! I can not access that channel')

        if len(data) < 50:
            return await msg.edit(content=f'An error has occured! Not enough data to create **Markov**')

        index = 1
        for word in data[index:]:
            key = data[index - 1]
            if key in chain:
                chain[key].append(word)
            else:
                chain[key] = [word]
            index += 1

        word1 = random.choice(list(chain.keys()))
        message = word1.capitalize()

        while len(message.split(' ')) < 12:
            word2 = random.choice(chain[word1])
            word1 = word2
            message += ' ' + word2

        await msg.edit(content=message)

    @commands.command(name='say', description='''Make me say a message or phrase.''',brief='Make me say a message', usage='say [text]', hidden=True)
    async def _say(self, ctx, *, msg: commands.clean_content(fix_channel_mentions=False, use_nicknames=False, escape_markdown=True)=None):
        if msg is None:
            msg = ctx.author.name
        await ctx.send(f'''{msg}'''.replace('@', '@\u200b'))

def setup(bot):
    bot.add_cog(Fun(bot))