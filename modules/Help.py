import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
from .utils import checks

class Help:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='help', description='Get command list or help with a certain command.', brief='Command list and help', usage='help [command]')
    async def _help(self, ctx, *, command=None):
        embed = discord.Embed(color=0x6441a5, description='mellow Bot made by mellowmarshe#0001. [Invite](https://discordapp.com/oauth2/authorize?client_id=496829853124001815&permissions=67439808&scope=bot)')
        embed.set_author(name=f'Help')
        embed.set_footer(text=f'For help with a command {ctx.prefix}help <command>')
        invalid_cogs = ('errorhandler')
        if command is None:
            if isinstance(command, commands.Group):
                subcommands = command.name
            for cog in self.bot.cogs.keys():
                if cog in invalid_cogs:
                    continue
                #cogs = [cog for cog in ctx.bot.cogs if ctx.bot.get_cog_commands(cog) and cog not in invalid_cogs]
                cmds = [c.name for c in self.bot.get_cog_commands(cog)]
                embed.add_field(name=f"{cog} ({len(cmds)} Commands)", value=", ".join(cmds))
        else:
            entity = self.bot.get_cog(command) or self.bot.get_command(command)
            if entity is None:
                clean = command.replace('@', '@\u200b')
                return await ctx.send(f'An error has occured! Command/Category not found: **{clean}**')
            else:
                if isinstance(command, commands.Group):
                    embed.add_field(name=command.name or "None", value=command.brief or "No help given")  
                if isinstance(entity, commands.Command):
                    embed.set_author(name=f'Help for {command}')
                    embed.add_field(name='Aliases', value=entity.aliases or "No aliases")
                    embed.add_field(name='Usage', value=entity.usage or "No entry")
                    embed.add_field(name=f'How to use', value=entity.description or "No help given")
                else:
                    if isinstance(command, commands.Group):
                        embed.add_field(name=command.name or "None", value=command.brief or "No help given")           
                    for cmd in self.bot.get_cog_commands(command):
                        embed.set_author(name=f'Help for category {command}')
                        embed.add_field(name=cmd.name or "None", value=cmd.brief or "No help given")
        await ctx.send(embed=embed)     

def setup(bot):
    bot.add_cog(Help(bot))