import discord
from discord.ext import commands
from discord.ext.commands import cooldown
from discord.ext.commands.cooldowns import BucketType
import asyncpg
from .utils import checks

class Starboard:
    def __init__(self, bot):
        self.bot = bot

    @commands.group(name='starboard', invoke_without_command=True, description='Main command used to invoke starboard commands', brief='Main starboard command', usage='starboard [subcommand]')
    async def _starboard(self, ctx):
        await ctx.send('An error has occured! You may not use this command without a subcommand.')

    @_starboard.command(name='configure', description='Setup starboard with this command', brief='Used to setup starboard', usage='starboard setup [#channel] [min_star=3] [selfstar=True/False]')
    @commands.has_permissions(manage_channels=True)
    async def _configure(self, ctx, channel: discord.TextChannel=None, min_star:int=None, selfstar=None):
        if min_star < 3:
            await ctx.send(f'An error has occured! May not set minumum star level below three(3). Setting minumum stars to three')
            min_star = 3
            pass
        if selfstar is not 'True' or 'False':
            await ctx.send(f'An error has occured! Self star rule can only be True or False. Setting self star rule to True (No self star)')
            selfstar = True
            pass
        if await self.bot.pool.fetchrow('''SELECT * FROM sb_guild_setting WHERE guildid = $1;''', ctx.guild.id) is None:
            await self.bot.pool.execute('''UPDATE sb_guild_setting SET sb_channel = $1, sb_min_limit = $2, sb_selfstar = $3 WHERE guildid = $4;''', channel.id, min_star, selfstar, ctx.guild.id)
        else:
            await self.bot.pool.execute('''INSERT INTO sb_guild_setting (sb_channel, sb_min_limit, sb_selfstar, guildid) VALUES ($1, $2, $3, $4);''', channel.id, min_star, selfstar, ctx.guild.id)
        await ctx.send(f'Set the starboard channel to {channel}, the minumum star count to {min_star}, and the self star rule to {selfstar}')
        await channel.send('Starboard channel test')
        
def setup(bot):
    bot.add_cog(Starboard(bot))